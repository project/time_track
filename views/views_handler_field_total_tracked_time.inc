<?php
class views_handler_field_total_tracked_time extends views_handler_field_numeric {
  function render($values) {
    $total = time_track_prepare_time(time_track_get_time($values->nid));
    return theme('time_track_format_time', $total);
  }
}