<?php
class views_handler_field_tracked extends views_handler_field_numeric {
 function construct() {
   parent::construct();
   $this->additional_fields['start'] = 'start';
   $this->additional_fields['stop'] = 'stop';
 }

 function render($values) {
   $start = $values->{$this->aliases['start']};
   $end = $values->{$this->aliases['stop']};
   $time = time_track_prepare_time($end - $start);
   return theme('time_track_format_time', $time);
 }
}