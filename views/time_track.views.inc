<?php
/**
 * Implementation of hook_views_data()
 */
function time_track_views_data() {
  $data['time_track']['table']['group']  = t('Node time tracker');

  $data['time_track']['table']['base'] = array(
    'field' => 'ttid',
    'title' => t('Time trackers'),
    'help' => t("Node time trackers."),
  );
  
  $data['time_track']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['time_track']['ttid'] = array(
    'title' => t('ID'),
    'help' => t('The time tracker ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['time_track']['uid'] = array(
    'title' => t('User'),
    'help' => t('The user who tracked time.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Users'),
     ),
  );

  $data['time_track']['nid'] = array(
    'title' => t('Node'),
    'help' => t('The node the time is tracked for.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );

  $data['time_track']['time'] = array(
    'title' => t('Tracked time'),
    'help' => t('The amount of time tracked for that node by that tracker.'),
    'field' => array(
      'handler' => 'views_handler_field_tracked_time',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );

  $data['time_track_log']['table']['group']  = t('Node time tracker logs');

  $data['time_track_log']['table']['base'] = array(
    'field' => 'ttid',
    'title' => t('Time trackers logs'),
    'help' => t("Node time tracker logs."),
  );

  $data['time_track_log']['table']['join'] = array(
    'time_track' => array(
      'left_field' => 'ttid',
      'field' => 'ttid',
    ),
  );

  $data['time_track_log']['ttlid'] = array(
    'title' => t('ID'),
    'help' => t('The time tracker log ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['time_track_log']['ttid'] = array(
    'title' => t('Time tracker ID'),
    'help' => t('The node time tracker ID the log is associated to.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'time_track',
      'field' => 'ttid',
      'label' => t('Node time tracker logs.'),
    ),
  );
  
  $data['time_track_log']['start'] = array(
    'title' => t('Start'),
    'help' => t('Time tracker log starting time'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['time_track_log']['stop'] = array(
    'title' => t('Stop'),
    'help' => t('Time tracker log stopping time'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}

function time_track_views_data_alter(&$data) {
  $data['node']['total_tracked'] = array(
    'title' => t('Total tracked time'),
    'help' => t('The total amount of time tracked for that node.'),
    'sort' => array(
      'handler' => 'views_handler_sort_total_tracked',
    ),
    'field' => array(
      'handler' => 'views_handler_field_total_tracked_time',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );

  // $data['time_track']['start'] = array(
  //   'title' => t('Time tracker starting time'),
  //   'help' => t('The starting time of that time tracker.'),
  //   'field' => array(
  //     'handler' => 'views_handler_field_time_track_start_time',
  //     'click sortable' => TRUE,
  //     'sort' => array(
  //       'handler' => 'views_handler_sort_numeric',
  //     ),
  //   ),
  //   'sort' => array(
  //     'handler' => 'views_handler_sort_date',
  //   ),
  //   'filter' => array(
  //     'handler' => 'views_handler_filter_date',
  //   ),
  // );

  $data['node']['form'] = array(
    'title' => t('Tracked form'),
    'help' => t('A form to start and stop time trackers.'),
    'field' => array(
      'handler' => 'views_handler_field_time_tracker_form',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function time_track_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'time_track') . '/views',
    ),
    'handlers' => array(
      'views_handler_field_total_tracked_time' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'views_handler_field_tracked_time' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_time_tracker_form' => array(
        'parent' => 'views_handler_field',
      ),
      // 'views_handler_field_time_track_start_time' => array(
      //   'parent' => 'views_handler_field_date',
      // ),
    ),
  );
}