<?php
class views_handler_field_time_track_start_time extends views_handler_field_date {
  function construct() {
    parent::construct();
    $this->additional_fields['ttid'] = 'ttid';
  }

  function render($values) {
    $result = db_fetch_object(db_query("SELECT MIN(start) FROM {time_track_log} WHERE ttid = %d", $values->{$this->aliases['ttid']}));
    $values->{$this->field_alias} = $result->start;
    parent::construct($values);
  }
}