The Time Track module allows you to track time on nodes. It integrates with Views and OG.

INSTALL
-------

1. Copy the time_track/ folder into your modules folder.

2. Go to http://plugins.jquery.com/project/jquery-countdown to download the jQuery Countdown plugin. Unzip and copy the
   jQuery Countdown folder in the time_track/ folder. Rename this folder as "jquery.countdown". You should have the following
   structure:

   + time_track
   |--+ jquery.countdown
      |-- jquery.countdown.js
      |-- ...
   |-- ...

3. Go to the modules page (admin/build/modules) and enable the Time Track module
   (dependency on the Views 2 module).

4. Navigate to the admin page of the content type for which you want to activate the Time Tracking
   feature (admin/content/types/YOUR_CONTENT_TYPE) and set "Time tracker: " to "Enabled".

5. Go to the user permissions page (admin/user/permissions) and define what your users can do:

   - "create any tracked time logs": allow to start/stop a time tracker for anybody
   - "create own tracked time logs": allow to start/stop a time tracker for oneself
   - "view any tracked time logs": allow to view all tracked time logs (not implemented yet)
   - "view own tracked time logs": allow to view own tracked time logs (not implemented yet)
   - "view any tracked time": allow to view tracked time of any user
   - "view own tracked time": allow to view own tracked time

   Note: "tracked time" is the sum of total amount of time tracked, while a "time track log" gathers starting and
   ending time of the time tracking.

6. Go to the block page (admin/build/block) and enable the "Time tracker" or the "Time tracking summary"
   block somewhere on the page.

7. On the node page (for the type which has the Time Tracking feature enabled), a block should appear offering the user
   to start and stop time tracking.

8. For OG support (mainly statistics consolidated for groups), you can enable the OG Time Track module.

AUTHOR
-------
Ronan Berder <hunvreus@gmail.com>
http://drupal.org/user/49057
http://teddy.fr