Drupal.behaviors.dynamicTimeTracker = function (context) {
  $('form.node-time-tracker', context).each(function () {
    new Drupal.timeTracker(this);
  });
};

/**
 * A time tracker switch object
 */
Drupal.timeTracker = function (form) {
  var tracker = this;
  this.form = form;
  this.nid = $('#'+ form.id +' input[name="nid"]').attr('value');
  this.uid = $('#'+ form.id +' input[name="uid"]').attr('value');
  this.button = $('#'+ form.id +' input[type="submit"]');
  this.url = Drupal.settings.time_tracker.url + '/' + this.nid + '/' + this.uid;
  this.counter = $('#'+ form.id +' .counter');
  
  this.initialize(); // TODO: make sure this function is called regularly to make sure trackers are in synch
};

/**
 * Initialize the time tracker
 */
Drupal.timeTracker.prototype.initialize = function () {
  var tracker = this;
  
  $.ajax({
    type: "GET",
    url: tracker.url,
    dataType: 'json',
    success: function (status) {
      $(tracker.counter).countdown({compact:true, since:-status['time']}).countdown('resume');

      if (status['status'] == 'ongoing') {
        $(tracker.button).toggle(
          function() {
            tracker.stopTracking();
            return false;
          },
          function() {
            tracker.startTracking();
            return false;
          }
        );
        $(tracker.counter).countdown('resume');
        $(tracker.button).val(Drupal.t('Stop'));
        $(tracker.form).removeClass('node-time-tracker-start').addClass('node-time-tracker-stop');
      }
      else {
        $(tracker.button).toggle(
          function() {
            tracker.startTracking();
            return false;
          },
          function() {
            tracker.stopTracking();
            return false;
          }
        );
        $(tracker.counter).countdown('pause');
        $(tracker.button).val(Drupal.t('Start'));
        $(tracker.form).removeClass('node-time-tracker-stop').addClass('node-time-tracker-start');
      } 
    },
    error: function (xmlhttp) {
      alert(Drupal.ahahError(xmlhttp, tracker.startURL));
    }
  });
};

/**
 * Starts time tracking
 */
Drupal.timeTracker.prototype.startTracking = function () {
  var tracker = this;
  
  // Ajax GET request for starting time tracker
  $.ajax({
    type: "GET",
    url: tracker.url + '/start',
    dataType: 'json',
    success: function (status) {
      $(tracker.counter).countdown('change', {since: -status['time']}).countdown('resume');
      $(tracker.button).val(Drupal.t('Stop'));
      $(tracker.form).removeClass('node-time-tracker-start').addClass('node-time-tracker-stop');
    },
    error: function (xmlhttp) {
      alert(Drupal.ahahError(xmlhttp, tracker.startURL));
    }
  });
};

/**
 * Stops time tracking
 */
Drupal.timeTracker.prototype.stopTracking = function () {
  var tracker = this;
  
  // Ajax GET request for stopping time tracker
  $.ajax({
    type: "GET",
    url: tracker.url + '/stop',
    dataType: 'json',
    success: function (status) {
      $(tracker.counter).countdown('change', {since: -status['time']}).countdown('pause');
      $(tracker.button).val(Drupal.t('Start'));
      $(tracker.form).removeClass('node-time-tracker-stop').addClass('node-time-tracker-start');
    },
    error: function (xmlhttp) {
      alert(Drupal.ahahError(xmlhttp, tracker.startURL));
    }
  });
};